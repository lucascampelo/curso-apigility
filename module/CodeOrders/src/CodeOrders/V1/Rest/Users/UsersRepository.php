<?php

namespace CodeOrders\V1\Rest\Users;

use Zend\Paginator\Adapter\DbTableGateway;
use Zend\Db\TableGateway\TableGateway;

class UsersRepository
{
    /**
     * @var TableGateway
     */
    private $tableGateway;

    /**
     * UsersRepository constructor.
     * @param TableGateway $tableGateway
     */
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * @return UsersCollection
     */
    public function findAll()
    {
        $paginatorAdapter = new DbTableGateway($this->tableGateway);

        return new UsersCollection($paginatorAdapter);
    }

    /**
     * @param int $id
     * @return object
     */
    public function find($id)
    {
        /** @var \Zend\Db\ResultSet\HydratingResultSet $resultSet */
        $resultSet = $this->tableGateway->select(['id' => (int) $id]);

        return $resultSet->current();
    }

    /**
     * @param $id
     * @return boolean
     */
    public function delete($id)
    {
        return (bool) $this->tableGateway->delete(['id' => (int) $id]);
    }

    /**
     * @param array $data
     * @return array
     */
    public function create($data)
    {
        $userMapper = new UsersMapper();
        $user = $userMapper->hydrate($data, new UsersEntity());
        return $this->tableGateway->insert( $userMapper->extract($user) );
    }

    /**
     * @param int $id
     * @param $data
     * @return int
     */
    public function update($id, $data)
    {
        return $this->tableGateway->update($data, ['id' => (int) $id]);
    }
}