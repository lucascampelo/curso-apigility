<?php
namespace CodeOrders\V1\Rest\Users;

use Zend\ServiceManager\ServiceManager;

class UsersResourceFactory
{
    /**
     * @param ServiceManager $services
     * @return UsersResource
     */
    public function __invoke($services)
    {
        return new UsersResource($services->get('CodeOrders\V1\Rest\Users\UsersRepository'));
    }
}
