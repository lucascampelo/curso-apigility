<?php

namespace CodeOrders\V1\Rest\Users;

use Zend\Stdlib\Hydrator\HydratorInterface;

class UsersMapper extends UsersEntity implements HydratorInterface
{

    /**
     * Extract values from an object
     *
     * @param UsersEntity $object
     * @return array
     */
    public function extract($object)
    {
        return [
            'id'         => $object->getId(),
            'username'   => $object->getUsername(),
            'password'   => $object->getPassword(),
            'first_name' => $object->getFirstName(),
            'last_name'  => $object->getLastName(),
            'role'       => $object->getRole()
        ];
    }

    /**
     * Hydrate $object with the provided $data.
     *
     * @param  array $data
     * @param  UsersEntity $object
     * @return UsersEntity
     */
    public function hydrate(array $data, $object)
    {
        $object->setId($data['id']);
        $object->setUsername($data['username']);
        $object->setPassword($data['password']);
        $object->setFirstName($data['first_name']);
        $object->setLastName($data['last_name']);
        $object->setRole($data['role']);

        return $object;
    }
}